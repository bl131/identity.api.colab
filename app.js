'use strict';

//allow viewing of the debug console when in debug mode
//if (process.env.NODE_ENV === 'development') {
process.env["DEBUG"] = 'connect:*';
//}
var fs = require('fs');
var https = require('https');
var SwaggerTools = require('swagger-tools');
var hotp = require('otplib');
var secretKey = require('./secretKey.js');
var storage = require('node-persist');
storage.initSync();//initalize storage for hotp count
var hotpcount = storage.getItem('hotpcount') || 0;

var config = {
  appRoot: __dirname // required config
};

var swaggerDoc = require('./api/swagger/swagger.json');

var app = require('connect')();

SwaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {

  // add hotp validation to only allow api syndication server access 
  if (process.env.NODE_ENV !== 'development') {
    app.use(check_signatures);
  }
  
  //only allow json resposes
  app.use(setJSONFormat);

  // install swagger middleware
  app.use(middleware.swaggerMetadata());
  //app.use(middleware.swaggerSecurity()); <= also not using this one
  app.use(middleware.swaggerValidator());
  app.use(catchValidationErrors);//elegantly catch validation errors
  app.use(middleware.swaggerRouter({
    controllers: './api/controllers',
    useStubs: false //process.env.NODE_ENV === 'development' ? true : false
  }));
  //app.use(middleware.swaggerUI()) <= let's not use this
  
  var key = process.env.NODE_ENV === 'development' ? fs.readFileSync('./ssl/snakeoil.key') : fs.readFileSync('./ssl/key.key');
  var cert = process.env.NODE_ENV === 'development' ? fs.readFileSync('./ssl/snakeoil.cert') : fs.readFileSync('./ssl/cert.cert');

  https.createServer({ key: key, cert: cert }, app).listen(443, function () {
    console.log('Listening on port ' + 443);
  });

});

function check_signatures(req, res, next) {
  var count = parseInt(req.headers['x-hotp-count']);
  if (!('x-hotp' in req.headers && 'x-hotp-count' in req.headers && hotpcount <= count && hotp.check(req.headers['x-hotp'], secretKey, count))) {
    hotpcount = count + 1; storage.setItem('hotpcount', hotpcount);
    res.writeHead(403, { "Content-Type": "application/json" });
    res.end('{"error":"Unauthorized access. HOTP signature invalid. You must go through the syndication server."}');
  }
}

function setJSONFormat(req, res, next) {
  res.setHeader('content-type', 'application/json');
  next();
}

function catchValidationErrors(err, req, res, next) {
  if ('failedValidation' in err && err.failedValidation) {
    res.statusCode = 400;
    res.end('{"error":"The request failed validation of its parameters and format. Check to make sure it conforms to the swagger spec for this api."}');
  }
}

