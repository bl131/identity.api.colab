'use strict';

require('es6-promise').polyfill();

//connect via ldaps and bind a connection pool to the colab-LDAP user
var ldap = require('ldapjs').createClient({
	url: 'ldaps://ldap.duke.edu',
	maxConnections: 1,
	bindDN: 'duLDAPKey=75798206-65fa-11e5-93e7-e04e1b144c1c,ou=People,ou=Test,dc=duke,dc=edu',
	bindCredentials: require('../config/password.js')
});

exports.getIdentityFromNetid = function (netid) {
	return new Promise(function (resolve, reject) {
		ldap.search('ou=people,dc=duke,dc=edu', {
			filter: 'uid=' + netid,
			scope: 'sub'
		}, function (err, res) {
			if (err) { reject(err); }

			res.on('searchEntry', function (entry) {
				//console.log('entry: ' + JSON.stringify(entry.object));
				if ('duDukeCardNbr' in entry.object) {
					delete entry.object.duDukeCardNbr;
				}
				resolve(entry.object);
			});
			res.on('searchReference', function (referral) {
				//console.log('referral: ' + referral.uris.join());
			});
			res.on('error', function (err) {
				console.error('error: ' + err.message);
				reject(err);
			});
			res.on('end', function (result) {
				//console.log('status: ' + result.status);
				resolve(null);
			});
		});
	});
};


exports.getIdentityFromCardNumber = function (num) {
	return new Promise(function (resolve, reject) {
		ldap.search('ou=people,dc=duke,dc=edu', {
			filter: 'duDukeCardNbr=' + num,
			scope: 'sub'
		}, function (err, res) {
			if (err) { reject(err); }

			res.on('searchEntry', function (entry) {
				//console.log('entry: ' + JSON.stringify(entry.object));
				if ('duDukeCardNbr' in entry.object) {
					delete entry.object.duDukeCardNbr;
				}
				var ob = entry.object;
				resolve({
					netid: 'uid' in ob ? ob['uid'] : '',
					firstName: 'givenName' in ob ? ob['givenName'] : '',
					lastName: 'sn' in ob ? ob['sn'] : '',
					gradYear: 'duPSExpGradTermC1' in ob ? ob['duPSExpGradTermC1'] : '',
					school: 'duPSAcadProgC1' in ob ? ob['duPSAcadProgC1'] : '',
					affiliations: 'eduPersonAffiliation' in ob ? ob['eduPersonAffiliation'] : ''
				});

			});
			res.on('searchReference', function (referral) {
				//console.log('referral: ' + referral.uris.join());
			});
			res.on('error', function (err) {
				console.error('error: ' + err.message);
				reject(err);
			});
			res.on('end', function (result) {
				//console.log('status: ' + result.status);
				resolve(null);
			});
		});
	});
};

exports.getCardNumberFromNetid = function (netid) {
	return new Promise(function (resolve, reject) {
		ldap.search('ou=people,dc=duke,dc=edu', {
			filter: 'uid=' + netid,
			scope: 'sub'
		}, function (err, res) {
			if (err) { reject(err); }

			res.on('searchEntry', function (entry) {
				//console.log('entry: ' + JSON.stringify(entry.object["duDukeCardNbr"]));
				resolve(entry.object["duDukeCardNbr"]);
			});
			res.on('searchReference', function (referral) {
				//console.log('referral: ' + referral.uris.join());
			});
			res.on('error', function (err) {
				console.error('error: ' + err.message);
				reject(err);
			});
			res.on('end', function (result) {
				//console.log('status: ' + result.status);
				resolve(null);
			});
		});
	});
};
