"use strict";
var controller = module.exports;
var ldapHelper = require('../helpers/ldap.js');
/* req has a swagger attribute, documented below:

                req.swagger

The structure of req.swagger is as follows:
- apiPath: string The API's path (The key used in the paths object for the corresponding API)
- path: object The corresponding path in the Swagger object that the request maps to
- operation: object The corresponding operation in the API Declaration that the request maps to
- operationParameters: object[] The computed parameters for this operation
- operationPath: string[] The path to the operation
- params: object For each of the request parameters defined in your Swagger document, its path, its schema and its processed value. (In the event the value needs coercion and it cannot be converted, the value property will be the original value provided.)
- security: object[] The computed security for this request
- swaggerObject: object The Swagger object
- useStubs: boolean Whether or not stubs have been turned on.
*/

// Operation swipeGet
// Parameters expected:
// num(Required)
controller["swipeGet"] = function(req,res) {
	var cardnum = req.swagger.params.num.value;
	
	ldapHelper.getIdentityFromCardNumber(cardnum).then(
		function (identity) {
			if (identity === null) {
				res.statusCode = 404;
				res.end('{}');
			}
			res.end(JSON.stringify(identity));
		},
		function (err) {
			res.end('{"error":"' + err + '"}');
		});
	
};